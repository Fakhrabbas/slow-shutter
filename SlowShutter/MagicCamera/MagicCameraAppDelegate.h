//
//  AppDelegate.h
//  MagicCamera
//
//  Created by i Pro on 2/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


#import "Configuration.h"

@class MenuViewController;

enum SE_TYPE {
    SE_SHUTTERSTART = 0,
    SE_SHUTTEREND,
};

@interface MagicCameraAppDelegate : UIResponder <UIApplicationDelegate> {
 	UINavigationController	*navigationController;
    AVAudioPlayer*		m_audioPlayer;
    NSArray*    _permissions;
    
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MenuViewController *viewController;

+ (MagicCameraAppDelegate *)sharedDelegate;

- (void)playSE:(int)type;
- (void)stopSE;
- (BOOL)isPlayingSE;

#pragma mark REBMOV

-(void)dispMoreGames;

#pragma mark Facebook
- (void)    FacebookLogin;
- (void)    FacebookLogOut;
- (void)    UploadPhotoToFacebook;

@end
